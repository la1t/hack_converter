from django.db import models
from main.constants import PropTypes


class SoloPattern(models.Model):
    col = models.IntegerField()
    row = models.IntegerField()

    class Meta:
        abstract = True


class StaticPattern(SoloPattern):
    convert = models.ForeignKey('main.ConvertRequest', on_delete=models.CASCADE, related_name='static_patterns')
    col = models.IntegerField()
    row = models.IntegerField()
    prop = models.CharField(max_length=100, choices=PropTypes.choices)
    prop_name = models.CharField(max_length=300, blank=True, null=True)
    delimiters = models.OneToOneField('patterns.ComposeDelimiter', on_delete=models.SET_NULL, blank=True, null=True)

    class Meta:
        ordering = ['convert', 'row', 'col']


class ComposeDelimiter(models.Model):
    row = models.CharField(max_length=100)
    col = models.CharField(max_length=100)


class DynamicPattern(models.Model):
    col_start = models.IntegerField()
    col_end = models.IntegerField()
    row_start = models.IntegerField()
    row_end = models.IntegerField()
    convert = models.ForeignKey('main.ConvertRequest', on_delete=models.CASCADE, related_name='dynamic_patterns')

    class Meta:
        ordering = ['convert', 'row_start', 'col_start']
