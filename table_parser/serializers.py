from rest_framework import serializers

from .models import Cell


class CellSerializer(serializers.ModelSerializer):
    val = serializers.SerializerMethodField()

    def get_val(self, instance):
        return instance.value if instance.value is not None else ''

    class Meta:
        model = Cell
        fields = ['val']


