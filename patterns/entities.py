from dataclasses import dataclass


class Static:
    pass


@dataclass
class SimpleMapping(Static):
    row: int
    col: int
    prop: str
    prop_name: str


@dataclass
class DynamicMapping:
    row: int
    col_start: int
    col_end: int

    def __eq__(self, other):
        if isinstance(other, DynamicMapping):
            return self.col_start == other.col_start
        return False
