import os
from datetime import datetime
from hashlib import md5


def upload_to_hash(instance, filename_src):
    now = datetime.now()

    ext = os.path.splitext(filename_src)[1]
    timestamp = str(now.time())
    encode_filename = md5((filename_src + timestamp).encode('utf-8')).hexdigest() + ext

    dir_path = '%d-%d/%d' % (now.year, now.month, now.day)

    return os.path.join(dir_path, encode_filename)
