from django.db import models


class Offer(models.Model):
    convert = models.ForeignKey('main.ConvertRequest', on_delete=models.CASCADE, related_name='offers')
    currency_id = 'RUB'
    name = models.TextField(blank=True)
    picture = ''
    category_id = ''
    begin_date = models.TextField(blank=True)
    end_date = models.TextField(blank=True)
    price = models.TextField(blank=True)
    model = models.TextField(blank=True)
    vendor = models.TextField(blank=True)
    vat = models.TextField(blank=True)


class Prop(models.Model):
    name = models.TextField()
    value = models.TextField(blank=True, null=True)

    offer = models.ForeignKey('offers.Offer', on_delete=models.CASCADE, related_name='props')
