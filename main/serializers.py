from rest_framework import serializers

from .models import ConvertRequest


class ConvertRequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = ConvertRequest
        fields = ['id', 'table_file', 'output_file', 'name', 'company', 'url', 'platform', 'version',
                  'agency', 'email']


class UploadTableSerializer(serializers.ModelSerializer):
    class Meta:
        model = ConvertRequest
        fields = ['id', 'table_file', 'output_file']


class GetRangeSerializer(serializers.Serializer):
    request_id = serializers.IntegerField()
    row_start = serializers.IntegerField()
    row_end = serializers.IntegerField()
    col_start = serializers.IntegerField()
    col_end = serializers.IntegerField()
