from django.db import models
from utils import upload_to_hash


class ConvertRequest(models.Model):
    first_offer = models.OneToOneField('main.FirstOfferRange', on_delete=models.SET_NULL, blank=True, null=True,
                                       related_name='convert')
    table_file = models.FileField(upload_to=upload_to_hash, blank=True, null=True)
    output_file = models.FileField(upload_to=upload_to_hash, blank=True, null=True)

    name = models.CharField(max_length=300)
    company = models.CharField(max_length=300)
    url = models.CharField(max_length=300, blank=True, null=True)
    platform = models.CharField(max_length=300, blank=True, null=True)
    version = models.CharField(max_length=300, blank=True, null=True)
    agency = models.CharField(max_length=300, blank=True, null=True)
    email = models.CharField(max_length=300, blank=True, null=True)

    def get_cell(self, row, col):
        return self.rows.get(index=row).cells.get(index=col)

    def get_range(self, row_start, col_start, row_end, col_end):
        rows = self.rows.filter(index__gte=row_start, index__lt=row_end)
        return [list(row.cells.filter(index__gte=col_start, index__lt=col_end)) for row in rows]

    def get_work_range(self):
        row_start = self.first_offer.row_start
        col_start = self.first_offer.col_start
        return self.get_range(row_start, col_start,
                              self.rows.count(), self.rows.first().cells.count())

    def get_first_offer_range(self):
        assert self.first_offer is not None
        return self.get_range(self.first_offer.row_start, self.first_offer.col_start, self.first_offer.row_end,
                              self.first_offer.col_end)


class FirstOfferRange(models.Model):
    row_start = models.IntegerField()
    row_end = models.IntegerField()
    col_start = models.IntegerField()
    col_end = models.IntegerField()
