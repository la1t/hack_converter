import xml.etree.ElementTree as ET
from datetime import datetime
from random import randint

from offers.models import Offer
from django.core.files.base import File

from .models import ConvertRequest


def xml_date(date: datetime):
    return date.strftime('%Y-%m-%d %H:%M')


class PropExtractor:

    def __init__(self, offer: Offer, parent):
        self.offer = offer
        self.parent = parent

    def add_prop(self, name, text, attrs=None):
        attrs = attrs or {}
        item = ET.SubElement(self.parent, 'param')
        item.attrib['name'] = name
        for attr_name, attr_value in attrs.values():
            item.attrib[attr_name] = attr_value
        item.text = str(text)

    def extract(self):
        for prop in self.offer.props.all():
            self.add_prop(prop.name, prop.value)


class ShopExtractor:

    def __init__(self, convert: ConvertRequest):
        self.convert = convert
        self.base = None

    def save_doc(self, doc):
        file_name = f'/tmp/{randint(0, 1000000)}.xml'
        doc.write(file_name, xml_declaration=True, short_empty_elements=False, encoding='utf-8')
        file = File(open(file_name))
        self.convert.output_file = file
        self.convert.save()

    def add_text_item(self, name, text, attrs=None):
        attrs = attrs or {}
        item = ET.SubElement(self.base, name)
        for attr_name, attr_value in attrs.values():
            item.attrib[attr_name] = attr_value
        item.text = str(text)

    def create_base_elements(self):
        root = ET.Element('yml_catalog')
        root.attrib['date'] = xml_date(datetime.now())
        doc = ET.ElementTree(root)
        shop = ET.SubElement(root, 'shop')
        self.base = shop
        return shop, doc

    def add_currencies(self, parent):
        currencies = ET.SubElement(parent, 'currencies')
        rub = ET.SubElement(currencies, 'currency')
        rub.attrib['id'] = 'RUB'
        rub.attrib['rate'] = '1'

    def add_categories(self, parent):
        categories = ET.SubElement(parent, 'categories')
        categories.text = ''

    def add_offers(self, parent):
        root = ET.SubElement(parent, 'offers')

        for offer in self.convert.offers.all():
            extractor = OfferExtractor(offer, root)
            extractor.extract()

    def extract(self):
        base, doc = self.create_base_elements()

        self.add_currencies(base)
        self.add_categories(base)
        self.add_offers(base)

        self.add_text_item('name', self.convert.name)
        self.add_text_item('company', self.convert.company)
        if self.convert.url:
            self.add_text_item('url', self.convert.url)
        if self.convert.platform:
            self.add_text_item('platform', self.convert.platform)
        if self.convert.version:
            self.add_text_item('version', self.convert.version)
        if self.convert.agency:
            self.add_text_item('agency', self.convert.agency)
        if self.convert.email:
            self.add_text_item('email', self.convert.email)

        self.save_doc(doc)


class OfferExtractor:

    def __init__(self, offer: Offer, parent):
        self.offer = offer
        self.parent = parent
        self.root = ET.SubElement(self.parent, 'offer')

    def add_text_item(self, name, text, attrs=None):
        attrs = attrs or {}
        item = ET.SubElement(self.root, name)
        for attr_name, attr_value in attrs.values():
            item.attrib[attr_name] = attr_value
        item.text = str(text)

    def extract(self):
        self.add_text_item('id', self.offer.id)
        self.add_text_item('name', self.offer.name)
        self.add_text_item('currencyId', self.offer.currency_id)
        self.add_text_item('picture', self.offer.picture)
        self.add_text_item('categoryId', self.offer.category_id)
        self.add_text_item('beginDate', self.offer.begin_date)
        self.add_text_item('beginEnd', self.offer.end_date)
        self.add_text_item('price', self.offer.price)
        self.add_text_item('model', self.offer.model)
        self.add_text_item('vendor', self.offer.vendor)
        self.add_text_item('vat', self.offer.vat)

        prop_extractor = PropExtractor(self.offer, self.root)
        prop_extractor.extract()
