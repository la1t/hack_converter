from django.urls import path, include

urlpatterns = [
    path('api/', include('main.urls')),
    path('api/', include('patterns.urls')),
]
