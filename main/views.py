from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from rest_framework.decorators import action
from rest_framework.parsers import MultiPartParser
from table_parser.serializers import CellSerializer
from table_parser.functions import parse_table
from patterns.functions import parse_convert

from .models import ConvertRequest, FirstOfferRange
from .serializers import GetRangeSerializer, ConvertRequestSerializer, UploadTableSerializer
from .extract import ShopExtractor


class ConvertRequestViewSet(ModelViewSet):
    serializer_class = ConvertRequestSerializer
    queryset = ConvertRequest.objects.all()

    @action(methods=['POST'], detail=True, parser_classes=(MultiPartParser,), url_path='upload-table')
    def upload_table(self, request, *args, **kwargs):
        convert = self.get_object()
        serializer = UploadTableSerializer(convert, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        parse_table(convert.table_file.path, convert)
        return Response(serializer.data)

    @action(methods=['POST'], detail=True)
    def trigger(self, request, *args, **kwargs):
        convert = self.get_object()
        parse_convert(convert)
        extractor = ShopExtractor(convert)
        extractor.extract()
        serializer = ConvertRequestSerializer(convert)
        return Response(serializer.data)


class GetRangeView(APIView):
    http_method_names = ['get']

    def get(self, request, *args, **kwargs):
        serializer = GetRangeSerializer(data=request.GET)
        serializer.is_valid(raise_exception=True)
        convert = ConvertRequest.objects.get(id=serializer.data['request_id'])
        cells_range = convert.get_range(serializer.data['row_start'], serializer.data['col_start'],
                                        serializer.data['row_end'], serializer.data['col_end'])
        ret = []
        for row in cells_range:
            ret.append([CellSerializer(cell).data for cell in row])
        return Response(ret)


class CreateFirstOfferView(APIView):
    http_method_names = ['post']
    serializer_class = GetRangeSerializer

    def post(self, request, *args, **kwargs):
        serializer = GetRangeSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = dict(serializer.validated_data)
        request_id = data.pop('request_id')
        first_offer = FirstOfferRange.objects.create(**data)
        convert = ConvertRequest.objects.get(id=request_id)
        convert.first_offer = first_offer
        convert.save()
        ret = []
        for row in convert.get_first_offer_range():
            ret.append([CellSerializer(cell).data for cell in row])
        return Response(ret)
