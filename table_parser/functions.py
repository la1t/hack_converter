import os

from openpyxl import load_workbook
from openpyxl.cell.cell import MergedCell
from django.conf import settings
from main.models import ConvertRequest

from .models import Row, Cell


def find_parent_cell(cell, merged_ranges, request):
    rows = request.rows.all()
    merge = None
    for m in merged_ranges:
        if m.min_col <= cell.column <= m.max_col and m.min_row <= cell.row <= m.max_row:
            merge = m
            break
    row = rows.get(index=merge.min_row - 1)
    return row.cells.get(index=merge.min_col - 1)


def parse_table(file_name, request):
    flush_request(request)

    workbook = load_workbook(file_name)
    sheet = workbook.worksheets[0]
    merged_ranges = sheet.merged_cells.ranges

    for (i, col) in enumerate(sheet.rows):
        row = Row.objects.create(request=request, index=i)

        for (j, cell) in enumerate(col):
            if isinstance(cell, MergedCell):
                is_merged = True
                parent = find_parent_cell(cell, merged_ranges, request)
                value = None
            else:
                is_merged = False
                parent = None
                value = cell.value
            row.cells.create(is_merged=is_merged, merge_parent=parent, value=value, index=j)

    return request


def parse_test_table():
    request = ConvertRequest.objects.create()
    return parse_table(os.path.join(settings.BASE_DIR, 'test_assets/table.xlsx'), request)


def flush_request(request):
    request.rows.all().delete()


def flush_requests():
    ConvertRequest.objects.all().delete()
