from django.urls import path

from . import views

urlpatterns = [
    path('static-patterns/', views.StaticPatternView.as_view(), name='static-patterns'),
    path('dynamic-patterns/', views.DynamicPatternView.as_view(), name='dynamic-patterns'),
]
