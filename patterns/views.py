from rest_framework.generics import CreateAPIView

from .models import StaticPattern, DynamicPattern
from .serializers import StaticPatternSerializer, DynamicPatternSerializer


class StaticPatternView(CreateAPIView):
    serializer_class = StaticPatternSerializer
    queryset = StaticPattern.objects.all()


class DynamicPatternView(CreateAPIView):
    serializer_class = DynamicPatternSerializer
    queryset = DynamicPattern.objects.all()
