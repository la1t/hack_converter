from django.apps import AppConfig


class TableParserConfig(AppConfig):
    name = 'table_parser'
