from rest_framework import serializers
from main.models import ConvertRequest

from .models import StaticPattern, DynamicPattern


class StaticPatternSerializer(serializers.ModelSerializer):
    convert_id = serializers.PrimaryKeyRelatedField(source='convert', queryset=ConvertRequest.objects.all(),
                                                    write_only=True)

    class Meta:
        model = StaticPattern
        fields = ['col', 'row', 'prop', 'prop_name', 'convert_id']


class DynamicPatternSerializer(serializers.ModelSerializer):
    convert_id = serializers.PrimaryKeyRelatedField(source='convert', queryset=ConvertRequest.objects.all(),
                                                    write_only=True)

    class Meta:
        model = DynamicPattern
        fields = ['col_start', 'col_end', 'row_start', 'row_end', 'convert_id']
