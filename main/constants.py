from django.db.models import TextChoices


class PropTypes(TextChoices):
    name = 'name', 'name'
    begin_data = 'beginDate', 'begin_date'
    end_date = 'endDate', 'end_date'
    price = 'price', 'price'
    model = 'model', 'model'
    vendor = 'vendor', 'vendor'
    vat = 'vat', 'vat'
    custom = 'custom', 'custom'
    compose = 'compose', 'compose'
