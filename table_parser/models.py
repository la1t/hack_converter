from django.db import models
from main.constants import PropTypes


class Row(models.Model):
    request = models.ForeignKey('main.ConvertRequest', on_delete=models.CASCADE, related_name='rows')
    index = models.IntegerField()

    def get_by_index(self, index):
        return self.cells.get(index=index)

    def __str__(self):
        return str(self.index)

    class Meta:
        ordering = ['request', 'index']


class Cell(models.Model):
    row = models.ForeignKey('table_parser.Row', on_delete=models.CASCADE, related_name='cells')
    index = models.IntegerField()
    is_merged = models.BooleanField(default=False)
    merge_parent = models.ForeignKey('self', on_delete=models.CASCADE, related_name='merged_cells', blank=True,
                                     null=True)
    value = models.TextField(blank=True, null=True)
    prop = models.CharField(max_length=100, blank=True, null=True, choices=PropTypes.choices)
    prop_name = models.CharField(max_length=300, blank=True, null=True)

    def __str__(self):
        return str(self.index)

    class Meta:
        ordering = ['row', 'index']
