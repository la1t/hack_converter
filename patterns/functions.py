from offers.models import Offer
from main.models import ConvertRequest
from main.constants import PropTypes

from .entities import SimpleMapping, DynamicMapping


def next_pattern(patterns, current_index):
    next_index = current_index + 1
    if next_index >= len(patterns):
        next_index = 0
    return next_index


def get_sub_patterns_on_row(convert: ConvertRequest, row_num):
    row_statics = []
    row_dynamics = []
    for static_pattern in convert.static_patterns.filter(row=row_num):
        row_statics.append(SimpleMapping(row=static_pattern.row, col=static_pattern.col, prop=static_pattern.prop,
                                         prop_name=static_pattern.prop_name))
    for dynamic_pattern in convert.dynamic_patterns.filter(row_start__lte=row_num, row_end__gt=row_num):
        row_dynamics.append(
            DynamicMapping(row=row_num, col_start=dynamic_pattern.col_start, col_end=dynamic_pattern.col_end)
        )
    return row_statics + row_dynamics


def filter_dynamic_mappings(patterns):
    return [pattern for pattern in patterns if isinstance(pattern, DynamicMapping)]


def get_sub_patterns(convert: ConvertRequest):
    ret = []

    rows_count = convert.first_offer.row_end - convert.first_offer.row_start
    row_num = 0
    while row_num < rows_count:
        patterns = get_sub_patterns_on_row(convert, row_num)
        dynamics = filter_dynamic_mappings(patterns)
        if len(dynamics) and row_num + 1 < rows_count:
            next_patterns = get_sub_patterns_on_row(convert, row_num + 1)
            while row_num + 1 < rows_count and ((dynamics == next_patterns) or not len(next_patterns)):
                row_num += 1
        ret.append(patterns)
        row_num += 1

    return ret


class AnalyzeException(Exception):
    pass


def analyze_row(row, patterns):
    for pattern in patterns:
        if isinstance(pattern, SimpleMapping):
            cell = row[pattern.col]
            if cell.value:
                cell.prop = pattern.prop
                cell.prop_name = pattern.prop_name
                cell.save()
            else:
                raise AnalyzeException()
        else:
            cells = row[pattern.col_start:pattern.col_end]
            prop_name = None
            for cell in cells:
                if cell.value:
                    prop_name = cell.value
                    break
            if prop_name is None:
                raise AnalyzeException()

            cells = cells[::-1]
            prop_value = ''
            for cell in cells:
                if cell.value:
                    prop_value = cell.value
                    cell.prop = PropTypes.custom
                    cell.prop_name = prop_name
                    break
            if not prop_value:
                cells[0].prop = PropTypes.custom
                cells[0].prop_name = prop_name
    return row


def add_prop(offer: Offer, cell):
    if cell.prop == PropTypes.custom:
        offer.props.create(name=cell.prop_name, value=cell.value)
    else:
        setattr(offer, cell.get_prop_display(), cell.value)


def create_offer(rows, convert):
    offer = Offer.objects.create(convert=convert)
    for row in rows:
        for cell in row:
            if cell.prop is not None:
                add_prop(offer, cell)
    offer.save()


def parse_convert(convert: ConvertRequest):
    convert.offers.all().delete()

    sub_patterns = get_sub_patterns(convert)

    current_index = 0
    current_sub_pattern = sub_patterns[current_index]
    current_offer = []

    work_range = convert.get_work_range()
    flush_style(work_range)

    row_num = 0
    while row_num < len(work_range):
        row = work_range[row_num]
        try:
            current_offer.append(analyze_row(row, current_sub_pattern))
            dynamic_mappings = filter_dynamic_mappings(current_sub_pattern)
            if len(dynamic_mappings):
                p_next_index = next_pattern(sub_patterns, current_index)
                p_next = sub_patterns[p_next_index]
                while True:
                    if row_num + 1 >= len(work_range):
                        break
                    next_row = work_range[row_num + 1]
                    try:
                        analyze_row(next_row, p_next)
                    except AnalyzeException:
                        current_offer.append(analyze_row(next_row, dynamic_mappings))
                        row_num = row_num + 1
                    else:
                        break
        except AnalyzeException:
            current_index = 0
            current_sub_pattern = sub_patterns[current_index]
            current_offer = []
        else:
            current_index = next_pattern(sub_patterns, current_index)
            current_sub_pattern = sub_patterns[current_index]
            if current_index == 0:
                create_offer(current_offer, convert)
                current_offer = []

        row_num += 1


def flush_style(rows):
    for row in rows:
        for cell in row:
            if cell.prop is not None:
                cell.prop = None
                cell.prop_name = None
