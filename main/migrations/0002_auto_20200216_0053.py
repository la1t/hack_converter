# Generated by Django 3.0.3 on 2020-02-16 00:53

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='FirstOfferRange',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('row_start', models.IntegerField()),
                ('row_end', models.IntegerField()),
                ('col_start', models.IntegerField()),
                ('col_end', models.IntegerField()),
            ],
        ),
        migrations.AddField(
            model_name='convertrequest',
            name='first_offer',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='convert', to='main.FirstOfferRange'),
        ),
    ]
