from django.urls import path, include
from rest_framework import routers

from . import views

router = routers.DefaultRouter()
router.register('convert-requests', views.ConvertRequestViewSet)

urlpatterns = [
    path('get-range/', views.GetRangeView.as_view(), name='get-range'),
    path('create-first-offer/', views.CreateFirstOfferView.as_view(), name='create-first-offer'),
    path('', include(router.urls)),
]
